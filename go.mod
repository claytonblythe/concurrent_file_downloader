module gitlab.com/claytonblythe/concurrent_file_downloader

go 1.12

require (
	github.com/tink-ab/tempfile v0.0.0-20180226111222-33beb0518f1a
	gitlab.com/claytonblythe/go_hello_world v0.0.0-20200306042735-c4b25f9ecc2e // indirect
)
